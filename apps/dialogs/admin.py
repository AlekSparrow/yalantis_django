from django.contrib import admin
from .models import Thread, Message


@admin.register(Thread)
class ThreadAdmin(admin.ModelAdmin):
    model = Thread
    can_delete = True
    readonly_fields = (
        "created_at",
        "updated_at",
    )
    list_display = ("pk", "get_participants", "created_at", "updated_at")
    search_fields = ("participants", "created_at", "updated_at")
    fieldsets = ((None, {"fields": ("participants",)}),)
    add_fieldsets = (None, {"classes": ("wide",), "fields": ("participants",)})


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    model = Message
    can_delete = True
    readonly_fields = (
        "created_at",
        "updated_at",
    )
    list_display = (
        "id",
        "sender",
        "thread",
        "text",
        "created_at",
        "updated_at",
        "is_read",
    )
    search_fields = ("sender", "thread", "created_at", "updated_at")
    list_filter = ("is_read",)
    fieldsets = ((None, {"fields": ("sender", "thread", "text", "is_read")}),)
    add_fieldsets = (
        None,
        {
            "classes": ("wide",),
            "fields": (
                "sender",
                "thread",
                "text",
                "is_read",
            ),
        },
    )
