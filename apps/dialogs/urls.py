from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import ThreadViewSet, MessageViewSet

app_name = "dialogs"
router = DefaultRouter()
router.register("thread", ThreadViewSet, basename="thread")
router.register("msg", MessageViewSet, basename="message")

urlpatterns = [] + router.urls
