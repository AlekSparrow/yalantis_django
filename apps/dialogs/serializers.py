from rest_framework import serializers
from .models import Thread, Message


class GetUserMixin:
    def get_user_from_request(self):
        return getattr(self.context.get("request"), "user", None)


class ThreadSerializer(GetUserMixin, serializers.ModelSerializer):
    class Meta:
        model = Thread
        fields = (
            "id",
            "participants",
            "created_at",
            "updated_at",
        )

    def validate_participants(self, value):
        user = self.get_user_from_request()
        if not user:
            raise serializers.ValidationError("Can't find user.")
        if not self.instance:
            if user not in value:
                raise serializers.ValidationError(
                    "Current user must be in participants. "
                    "Or You'r can't remove from the thread someone else."
                )
            if len(value) > 2:
                raise serializers.ValidationError("You can add two participants only.")
            if len(value) < 2:
                raise serializers.ValidationError("Please add one more participants.")
        return value

    def update(self, instance, validated_data):
        participant_for_remove = self.get_user_from_request()
        if not participant_for_remove:
            raise serializers.ValidationError("Not found.")
        instance.participants.remove(participant_for_remove)
        if instance.participants.count() < 1:
            instance.delete()
            return instance
        return instance


class MessageSerializer(GetUserMixin, serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = (
            "id",
            "sender",
            "thread",
            "text",
            "created_at",
            "updated_at",
            "is_read",
        )

    def validate_thread(self, value):
        user = self.get_user_from_request()
        if not user:
            raise serializers.ValidationError("Can't find user.")
        if user.email not in value.get_participants():
            raise serializers.ValidationError("Current user must be in participants.")
        return value

    def create(self, validated_data):
        sender = self.get_user_from_request()
        validated_data.update(sender=sender)
        message = super().create(validated_data=validated_data)
        return message

    def update(self, instance, validated_data):
        if instance.sender != self.get_user_from_request():
            raise serializers.ValidationError("Your can't edit this message")
        text_data = validated_data.pop("text", "")
        if not text_data:
            return instance
        instance.text = text_data
        return instance

    def destroy(self, instance):
        if instance.sender != self.get_user_from_request():
            raise serializers.ValidationError("Your can't delete this message")
        else:
            instance.delete()
            return instance


class ThreadListSerializer(GetUserMixin, serializers.ModelSerializer):
    last_message = serializers.SerializerMethodField()
    unreaded_messages = serializers.SerializerMethodField()

    class Meta:
        model = Thread
        fields = (
            "id",
            "participants",
            "last_message",
            "unreaded_messages",
            "created_at",
            "updated_at",
        )

    @staticmethod
    def get_last_message(obj):
        instance = obj.thread.last()
        return MessageSerializer(instance=instance).data if instance else None

    @staticmethod
    def get_unreaded_messages(obj):
        return obj.thread.unread(obj).count()


class MessageListSerializer(GetUserMixin, serializers.ModelSerializer):
    is_read = serializers.SerializerMethodField()

    class Meta:
        model = Message
        fields = (
            "id",
            "sender",
            "thread",
            "text",
            "created_at",
            "updated_at",
            "is_read",
        )

    def get_is_read(self, obj):
        if not obj.is_read and obj.sender == self.get_user_from_request():
            obj.is_read = True
            obj.save()
        return obj.is_read
