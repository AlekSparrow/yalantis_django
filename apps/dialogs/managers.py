from django.db import models


class MessageManager(models.Manager):
    def read(self, thread):
        return super().get_queryset().filter(is_read=True, thread=thread)

    def unread(self, thread):
        return super().get_queryset().filter(is_read=False, thread=thread)
