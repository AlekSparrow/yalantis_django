from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsParticipants(BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.participants.filter(email=request.user.email)


class IsSenderOrReadOnly(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return obj.sender == request.user
